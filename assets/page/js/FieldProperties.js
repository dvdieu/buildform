// String.js - liberated from MicrosoftAjax.js on 03/28/10 by Sky Sanders
// permalink: http://stackoverflow.com/a/2534834/2343

/*
 Copyright (c) 2009, CodePlex Foundation
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted
 provided that the following conditions are met:

 *   Redistributions of source code must retain the above copyright notice, this list of conditions
 and the following disclaimer.

 *   Redistributions in binary form must reproduce the above copyright notice, this list of conditions
 and the following disclaimer in the documentation and/or other materials provided with the distribution.

 *   Neither the name of CodePlex Foundation nor the names of its contributors may be used to endorse or
 promote products derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</textarea>
 */

(function(window) {

	$type = String;
	$type.__typeName = 'String';
	$type.__class = true;

	$prototype = $type.prototype;
	$prototype.endsWith = function String$endsWith(suffix) {
		/// <summary>Determines whether the end of this instance matches the specified string.</summary>
		/// <param name="suffix" type="String">A string to compare to.</param>
		/// <returns type="Boolean">true if suffix matches the end of this instance; otherwise, false.</returns>
		return (this.substr(this.length - suffix.length) === suffix);
	}

	$prototype.startsWith = function String$startsWith(prefix) {
		/// <summary >Determines whether the beginning of this instance matches the specified string.</summary>
		/// <param name="prefix" type="String">The String to compare.</param>
		/// <returns type="Boolean">true if prefix matches the beginning of this string; otherwise, false.</returns>
		return (this.substr(0, prefix.length) === prefix);
	}

	$prototype.trim = function String$trim() {
		/// <summary >Removes all leading and trailing white-space characters from the current String object.</summary>
		/// <returns type="String">The string that remains after all white-space characters are removed from the start and end of the current String object.</returns>
		return this.replace(/^\s+|\s+$/g, '');
	}

	$prototype.trimEnd = function String$trimEnd() {
		/// <summary >Removes all trailing white spaces from the current String object.</summary>
		/// <returns type="String">The string that remains after all white-space characters are removed from the end of the current String object.</returns>
		return this.replace(/\s+$/, '');
	}

	$prototype.trimStart = function String$trimStart() {
		/// <summary >Removes all leading white spaces from the current String object.</summary>
		/// <returns type="String">The string that remains after all white-space characters are removed from the start of the current String object.</returns>
		return this.replace(/^\s+/, '');
	}

	$type.format = function String$format(format, args) {
		/// <summary>Replaces the format items in a specified String with the text equivalents of the values of   corresponding object instances. The invariant culture will be used to format dates and numbers.</summary>
		/// <param name="format" type="String">A format string.</param>
		/// <param name="args" parameterArray="true" mayBeNull="true">The objects to format.</param>
		/// <returns type="String">A copy of format in which the format items have been replaced by the   string equivalent of the corresponding instances of object arguments.</returns>
		return String._toFormattedString(false, arguments);
	}

	$type._toFormattedString = function String$_toFormattedString(useLocale, args) {
		var result = '';
		var format = args[0];

		for (var i = 0; ; ) {
			// Find the next opening or closing brace
			var open = format.indexOf('{', i);
			var close = format.indexOf('}', i);
			if ((open < 0) && (close < 0)) {
				// Not found: copy the end of the string and break
				result += format.slice(i);
				break;
			}
			if ((close > 0) && ((close < open) || (open < 0))) {

				if (format.charAt(close + 1) !== '}') {
					throw new Error('format stringFormatBraceMismatch');
				}

				result += format.slice(i, close + 1);
				i = close + 2;
				continue;
			}

			// Copy the string before the brace
			result += format.slice(i, open);
			i = open + 1;

			// Check for double braces (which display as one and are not arguments)
			if (format.charAt(i) === '{') {
				result += '{';
				i++;
				continue;
			}

			if (close < 0) throw new Error('format stringFormatBraceMismatch');


			// Find the closing brace

			// Get the string between the braces, and split it around the ':' (if any)
			var brace = format.substring(i, close);
			var colonIndex = brace.indexOf(':');
			var argNumber = parseInt((colonIndex < 0) ? brace : brace.substring(0, colonIndex), 10) + 1;

			if (isNaN(argNumber)) throw new Error('format stringFormatInvalid');

			var argFormat = (colonIndex < 0) ? '' : brace.substring(colonIndex + 1);

			var arg = args[argNumber];
			if (typeof (arg) === "undefined" || arg === null) {
				arg = '';
			}

			// If it has a toFormattedString method, call it.  Otherwise, call toString()
			if (arg.toFormattedString) {
				result += arg.toFormattedString(argFormat);
			}
			else if (useLocale && arg.localeFormat) {
				result += arg.localeFormat(argFormat);
			}
			else if (arg.format) {
				result += arg.format(argFormat);
			}
			else
				result += arg.toString();

			i = close + 1;
		}

		return result;
	}

})(window);

function getFieldByKey(key)
{
	return DataFP[key];
}

function getFieldTime(flag)
{
	var str = '<option value="-1">None</option>';
	if(flag===true)
	{
		for(var i=0;i<=9;i++)
		{

			str+='<option value='+i+'>0'+i+':'+'00'+'</option>';
		}
		for(var i=10;i<=23;i++)
		{

			str+='<option value='+i+'>'+i+':'+'00'+'</option>';
		}
	}
	else
	{
		for(var i=0;i<=9;i++)
		{

			str+='<option value='+i+'>0'+i+':'+'00'+'</option>';
		}
		for(var i=10;i<=22;i++)
		{

			str+='<option value='+i+'>'+i+':'+'00'+'</option>';
		}
	}
	return str;
}

function getFieldChoise(value,title="")
{
	var str = "";
	if(value>0) {
		for(var i=0;i<value;i++)
		{
			str += '<li>'+
				'<div class="input-group col-sm-1">'+
						'<input type="radio" name="radio">'+
				'</div>'+
				'<div class="input-group col-sm-6">'+
						'<input type="text" class="form-control" placeholder='+title+'>'+
				'</div>'+
				'<div class="input-group col-sm-1">'+
						'<button type="button" class="btn btn-default addmoreChoise"><i class="fa fa-plus" aria-hidden="true"></i></button>'+
				'</div>'+
				'<div class="input-group col-sm-1">'+
						
						'<button type="button" class="btn btn-default removeChoise"><i class="fa fa-minus" aria-hidden="true"></i></button>'+
				'</div>'+
				'<div class="input-group col-sm-1">'+
						'<i class="fa fa-arrows" aria-hidden="true"></i>'+
				'</div>'+
	'</li>';
		}
		return str;	
	}
	else
	{
		return null;
	}	
}

function getFieldCheckBox(value,title="")
{	
	var str = "";
	if(value>0) {
		for(var i=0;i<value;i++)
		{
			str += '<li>'+
				'<div class="input-group col-sm-1">'+
						'<input type="checkbox">'+
				'</div>'+
				'<div class="input-group col-sm-6">'+
						'<input type="text" class="form-control" placeholder='+title+'>'+
				'</div>'+
				'<div class="input-group col-sm-1">'+
						'<button type="button" class="btn btn-default addmoreChoise"><i class="fa fa-plus" aria-hidden="true"></i></button>'+
				'</div>'+
				'<div class="input-group col-sm-1">'+
						
						'<button type="button" class="btn btn-default removeChoise"><i class="fa fa-minus" aria-hidden="true"></i></button>'+
				'</div>'+
				'<div class="input-group col-sm-1">'+
						'<i class="fa fa-arrows" aria-hidden="true"></i>'+
				'</div>'+
	'</li>';
		}
		return str;	
	}
	else
	{
		return null;
	}	
}

var dataJSON ='{"fieldBasic":{"name":"txtInput1","class":"form-control","type":"text","placeholder":"Số Điện Thoại"},"fieldinfo":{"label":true,"labelvalue":"vn","value":"Số Điện Thoại","decreption":true,"decreptionvalue":"Chỉ dẫn khách hàng"}}';

function getTextBox(dataBasic)
{
	var jsonData = JSON.parse(dataBasic);
	console.log(jsonData);
	var fieldBasic = jsonData.fieldBasic;
	var name=fieldBasic.name;
	var className = fieldBasic.class;
	var id = fieldBasic.id;
	var value = fieldBasic.value;
	var placeholder = fieldBasic.placeholder;
	var type = fieldBasic.type;
    var fieldinfo = jsonData.fieldinfo;
    if(fieldinfo !==undefined)
    {
        if(fieldinfo.label)
        {
            var item = String.format('<label>'+fieldinfo.labelvalue+'</label>'+'<input type="{0}" class="{1}" name="{2}" id ="{3}" value="{4}" placeholder="{5}">',type,className,name,id,value,placeholder);
        }
    } else {
            var item = String.format('<input type="{0}" class="{1}" name="{2}" id ="{3}" value="{4}" placeholder="{5}">', type, className, name, id, value, placeholder);
    }
	return item;
}

function getSelectBox(dataBasic,data) {

}

DataFP = {
    'fieldname':'<div class="form-group">'+
    '<label>' + 'Field Name' + '</label>'+
    '<input type="text" class="form-control" placeholder="' + 'Field Name' + '">'+
    '</div>',

'fielddelugename':'<div class="form-group">'+
'<label>' + 'Field Deluge Name' + '</label>'+
'<input type="text" class="form-control" placeholder="' + 'Field Deluge Name' + '">'+
'</div>',

    'placeholder':'<div class="form-group">'+
    '<label>' + 'Placeholder' + '</label>'+
    '<input type="text" class="form-control" placeholder="' + 'Placeholder' + '">'+
    '</div>',

'mandatory':'<div class="form-group">'+
'<label class="checkbox-inline">'+
  '<input type="checkbox" id="inlineCheckbox1" value="option1"> Mandatory'+
'</label>'+
'</div>',

'noduplicatevalues':'<div class="form-group">'+
'<label class="checkbox-inline">'+
  '<input type="checkbox" id="inlineCheckbox1" value="option1"> No Duplicate Values'+
'</label>'+
'</div>',

'initialvalue':'<div class="form-group">'+
'<label>' + 'Initial Value' + '</label>'+
'<input type="text" class="form-control" placeholder="' + 'Initial Value' + '">'+
'</div>',

'maxcharacters':'<div class="form-group">'+
'<label>' + 'Max Characters' + '</label>'+
'<input type="text" class="form-control" id="maxcharacters">'+
'</div>',

'heightupdown':'<div class="form-group">'+
'<label>' + 'Height' + '</label>'+
'<input type="text" class="form-control" id="heightupdown">'+
'</div>',

'showfieldto':
'<div class="form-group">'+
'<label>' + 'Show Field To' + '</label>'+
'<select class="selectpicker form-control">'+
  '<option>Everyone</option>'+
  '<option>Admin</option>'+
'</select>'+
'</div>',

'fieldtype':
'<div class="form-group">'+
'<label class="selectpicker form-control">' + 'Field Type' + '</label>'+
'<select  >'+
  '<option>Single Line</option>'+
  '<option>Multi Line</option>'+
  '<option>Rich Text</option>'+
'</select>'+
'</div>',

'fieldtypemultiline':
'<div class="form-group">'+
'<label>' + 'Field Type' + '</label>'+
'<select class="selectpicker">'+
  '<option>Multi Line</option>'+
  '<option>Rich Text</option>'+
'</select>'+
'</div>',

'fieldtypeemail':
'<div class="form-group">'+
'<label>' + 'Field Type' + '</label>'+
'<select class="selectpicker form-control">'+
  '<option>Email</option>'+
  '<option>Singline</option>'+
'</select>'+
'</div>',

'fieldtypedropdown':
'<div class="form-group">'+
'<label>' + 'Field Type' + '</label>'+
'<select class="selectpicker form-control">'+
  '<option>Drop Down</option>'+
  '<option>Radio</option>'+
'</select>'+
'</div>',


'fieldtypecheckbox':
'<div class="form-group">'+
'<label>' + 'Field Type' + '</label>'+
'<select class="selectpicker form-control">'+
  '<option>Checkbox</option>'+
  '<option>Multi Select</option>'+
'</select>'+
'</div>',

'fieldsize':
'<div class="form-group">'+
'<label>' + 'Field Size' + '</label>'+
'<select class="selectpicker form-control">'+
  '<option>Small</option>'+
  '<option>Medium</option>'+
  '<option>Large</option>'+
  '<option>Custom</option>'+
'</select>'+
'</div>',

'fieldtypeupload':
'<div class="form-group">'+
'<label>' + 'Field Size' + '</label>'+
'<select class="selectpicker form-control">'+
  '<option>File upload</option>'+
'</select>'+
'</div>',

'sizeupdown':'<div class="form-group" style="display: none;">'+
'<input type="text" class="form-control col-sm-2" id="maxsize">'+
'</div>',

'initialdate':'<div class="form-group">'+
'<label>' + 'Initial Value' + '</label>'+
'<input type="text" class="form-control date-picker" placeholder="' + 'dd-MM-yyyy' + '">'+
'</div>',

'allowedday':'<div class="form-group">'+
'<label">' + 'Allowed Days' + '</label>'+
'<select class="allowedday form-control" multiple data-size="5" title="Choose ..." data-actions-box="true" data-container="body">'+
  '<option title="Sun" value="1">Sunday</option>'+
  '<option title="Mon" value="2">Monday</option>'+
  '<option title="Tue" value="3">Tuesday</option>'+
  '<option title="Wed" value="4">Wednesday</option>'+
  '<option title="Thu" value="5">Thursday</option>'+
  '<option title="Fri" value="6">Friday</option>'+
  '<option title="Sat" value="7">Saturday</option>'+
'</select>'+
'</div>',

'initialdatetime':'<div class="form-group">'+
'<label>' + 'Initial Value' + '</label>'+
'<input type="text" value="" class="form-control form_datetime" placeholder="dd-MM-yyyy 00:00" readonly>'+
'</div>',

'allowedhours':'<div class="form-group">'+
'<label>' + 'Allowed Hours' + '</label>'+
'</div>'+
'<div class="form-group">'+
'<select class="allowedhoursstart">'+
getFieldTime(false)+
'</select>'+
'<label">' + ' to ' + '</label>'+
'<select class="allowedhoursend form-control">'+
getFieldTime(true)+
'</select>'+
'</div>',

'choices':
'<div class="form-inline">'+
'<ul class="dropdown-sortable">'+
	getFieldChoise(3)+
'</ul>'+
'</div>',

'checkbox':
'<div class="form-inline">'+
'<ul class="dropdown-sortable">'+
	getFieldCheckBox(3)+
'</ul>'+
'</div>',

'alphabeticalorder':'<div class="form-group">'+
'<label class="checkbox-inline">'+
  '<input type="checkbox" id="inlineCheckbox1" value="option1"> Alphabetical Order'+
'</label>'+
'</div>',

'allowotherchoice':'<div class="form-group">'+
'<label class="checkbox-inline">'+
  '<input type="checkbox" id="inlineCheckbox1" value="option1"> Allow Other Choice'+
'</label>'+
'</div>',

'maxdigits':'<div class="form-group">'+
'<label>' + 'Max Digits' + '</label>'+
'<input type="text" class="form-control" id="maxcharacters">'+
'</div>',

'decimalpoints':'<div class="form-group">'+
'<label>' + 'Decimal Points' + '</label>'+
'<input type="text" class="form-control" id="maxcharacters">'+
'</div>',

'fieldtypenumber':
'<div class="form-group">'+
'<label>' + 'Field Type' + '</label>'+
'<select class="selectpicker">'+
  '<option>Number</option>'+
  '<option>Decimal</option>'+
  '<option>Percent</option>'+
  '<option>Currency</option>'+
'</select>'+
'</div>',	

'fieldtypedecimal':
'<div class="form-group">'+
'<label>' + 'Field Type' + '</label>'+
'<select class="selectpicker">'+
  '<option>Decimal</option>'+
  '<option>Percent</option>'+
  '<option>Currency</option>'+
  '<option>Number</option>'+
'</select>'+
'</div>',	

'fieldtypepercent':
'<div class="form-group">'+
'<label>' + 'Field Type' + '</label>'+
'<select class="selectpicker form-control">'+
  '<option>Percent</option>'+
  '<option>Decimal</option>'+
  '<option>Currency</option>'+
'</select>'+
'</div>',	

'fieldtypecurrency':
'<div class="form-group">'+
'<label>' + 'Field Type' + '</label>'+
'<select class="selectpicker form-control">'+
  '<option>Currency</option>'+
  '<option>Decimal</option>'+
  '<option>Percent</option>'+
'</select>'+
'</div>',

'fieldtypeurl':
'<div class="form-group">'+
'<label >' + 'Field Type' + '</label>'+
'<select  >'+
  '<option>Currency</option>'+
  '<option>Decimal</option>'+
  '<option>Percent</option>'+
'</select>'+
'</div>',	

'target':
'<div class="form-group">'+
'<label >' + 'Target' + '</label>'+
'<select class="selectpicker form-control">'+
  '<option>New Window</option>'+
  '<option>Same Window</option>'+
'</select>'+
'</div>',

'urllinkname':'<div class="form-group">'+
'<label class="checkbox-inline">'+
  '<input type="checkbox" id="inlineCheckbox1" value="option1"> Link Name'+
'</label>'+
'</div>',

'urltitle':'<div class="form-group">'+
'<label class="checkbox-inline">'+
  '<input type="checkbox" id="inlineCheckbox1" value="option1"> Title'+
'</label>'+
'</div>',

'urlalt':'<div class="form-group">'+
'<label class="checkbox-inline">'+
  '<input type="checkbox" id="inlineCheckbox1" value="option1"> Title'+
'</label>'+
'</div>',

'googledocs':'<div class="form-group">'+
'<label class="checkbox-inline">'+
  '<input type="checkbox" id="inlineCheckbox1" value="option1"> Google Docs'+
'</label>'+
'</div>',

'imglink':'<div class="form-group">'+
'<label class="checkbox-inline">'+
  '<input type="checkbox" id="inlineCheckbox1" value="option1"> Link '+
'</label>'+
'</div>',

'imglocalcomputer':'<div class="form-group">'+
'<label class="checkbox-inline">'+
  '<input type="checkbox" id="inlineCheckbox1" value="option1"> Local Computer '+
'</label>'+
'</div>',

'notes':'<div class="form-group">'+
'<label>' + 'Field name' + '</label>'+
'<input type="textarea" class="form-control" placeholder="' + 'Notes' + '">'+
'</div>',

}
/* 
* Group Field
*/
var group = {
'singleline':'<div class="card-panel">' +
    '<div class="card-panel">' +

'</div>'+
'<div class="card-panel">'+

'<h3 class="panel-title">Panel title</h3>'+
'<select class="selectpicker">'+
'<option>Tiếng Việt</option>'+
'<option>English</option>'+
'<option>Tiếng Thái</option>'+
'</select>'+
    '<button type="button" class="btn btn-info btn-circle" style=" width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1.428571429; border-radius: 15px;"><i class="fa fa-plus"></i></button>'+
'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+getFieldByKey('placeholder')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+getFieldByKey('noduplicatevalues')+
getFieldByKey('initialvalue')+
getFieldByKey('maxcharacters')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtype')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>'
,
 
'multiline':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +	
'<div class="panel-body">'+getFieldByKey('fieldtypemultiline')+getFieldByKey('fieldsize')+getFieldByKey('heightupdown')+	
'</div>'+
'</div>',

'email':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+getFieldByKey('noduplicatevalues')+
getFieldByKey('initialvalue')+
getFieldByKey('maxcharacters')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtypeemail')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>',

'richtext':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +	
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +	
'<div class="panel-body">'+getFieldByKey('fieldtypemultiline')+getFieldByKey('fieldsize')+getFieldByKey('heightupdown')+	
'</div>'+
'</div>',

'date':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+getFieldByKey('noduplicatevalues')+
getFieldByKey('initialdate')+
getFieldByKey('allowedday')+
getFieldByKey('maxcharacters')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtype')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>'
,

'datetime':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+getFieldByKey('noduplicatevalues')+
getFieldByKey('initialdatetime')+
getFieldByKey('allowedday')+
getFieldByKey('allowedhours')+
getFieldByKey('maxcharacters')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtype')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>',

'dropdown':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Choices'+'</div>' +
'<div class="panel-body">'+getFieldByKey('choices')+
getFieldByKey('alphabeticalorder')+getFieldByKey('allowotherchoice')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtype')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>',

'radio':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Choices'+'</div>' +
'<div class="panel-body">'+getFieldByKey('choices')+
getFieldByKey('alphabeticalorder')+getFieldByKey('allowotherchoice')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtype')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>',

'checkbox':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Choices'+'</div>' +
'<div class="panel-body">'+getFieldByKey('checkbox')+
getFieldByKey('alphabeticalorder')+getFieldByKey('allowotherchoice')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtypecheckbox')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>',

'multiselect':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Choices'+'</div>' +
'<div class="panel-body">'+getFieldByKey('checkbox')+
getFieldByKey('alphabeticalorder')+getFieldByKey('allowotherchoice')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtypecheckbox')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>',

'number':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+getFieldByKey('noduplicatevalues')+
getFieldByKey('initialvalue')+
getFieldByKey('maxdigits')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtypenumber')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>'
,

'decimal':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+getFieldByKey('noduplicatevalues')+
getFieldByKey('initialvalue')+
getFieldByKey('maxdigits')+
getFieldByKey('decimalpoints')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtypedecimal')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>'
,

'percent':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+getFieldByKey('noduplicatevalues')+
getFieldByKey('initialvalue')+
getFieldByKey('maxdigits')+
getFieldByKey('decimalpoints')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtypepercent')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>'
,

'currency':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+getFieldByKey('noduplicatevalues')+
getFieldByKey('initialvalue')+
getFieldByKey('maxdigits')+
getFieldByKey('decimalpoints')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtypecurrency')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
'</div>'+
'</div>'
,
'url':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtypeurl')+getFieldByKey('fieldsize')+getFieldByKey('sizeupdown')+
getFieldByKey('target')+
getFieldByKey('urllinkname')+
getFieldByKey('urltitle')+
'</div>'+
'</div>'
,

'fileupload':'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Field Info'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldname')+getFieldByKey('fielddelugename')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Browse Options'+'</div>' +
'<div class="panel-body">'+getFieldByKey('imglocalcomputer')+getFieldByKey('googledocs')+
'</div>'+
'</div>'+

'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Validation'+'</div>' +
'<div class="panel-body">'+getFieldByKey('mandatory')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Permission'+'</div>' +
'<div class="panel-body">'+getFieldByKey('showfieldto')+
'</div>'+
'</div>'+
'<div class="panel panel-default">' +
'<div class="panel-heading">'+'Appearance'+'</div>' +
'<div class="panel-body">'+getFieldByKey('fieldtypeupload')+
getFieldByKey('target')+
getFieldByKey('urllinkname')+
getFieldByKey('urltitle')+
'</div>'+
'</div>'
,
}
