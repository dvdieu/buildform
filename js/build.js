var single=false;
$(function () {
	var flagAdd = false;
	$(".ui-draggable").draggable({
		cancel: true, helper: "clone", zIndex: 9999, connectToSortable: ".ui-sortable"
	});

	$("#fieldList").sortable({
		placeholder: "selectedType",
		start: function (e, ui) {
			ui.placeholder.height(ui.item.innerHeight());
			ui.item.css('opacity', '0.6');
			$('.scroll').html('');
			$('.scroll').html('<div id="contentFP" style="max-height: 600px;"></div>');
		},
		stop: function (e, ui) {
			ui.item.css('opacity', '1');
			if (flagAdd === true) {
				ui.item.remove();
				flagAdd = false;
			}
			$(".selectedType").removeClass("selectedType");
			ui.item.addClass("selectedType");
			single=false;
		},
		receive: function (e, ui) {
			flagAdd = true;
			for (var i = 0; i < data.length; i++) {
				if (data[i].key === ui.item.attr('fieldtype')) {
					if (ui.item.attr('fieldtype') === "lookup") {
						$('body').append(popup);
					}
					$(this).append(data[i].value);
					return true;
				}
			}
		}
	});
});
$(document).on('click', '.tempFrmWrapper', function () {
	$(".tempFrmWrapper").removeClass("selectedType");
	$(this).addClass("selectedType");
	if(single===false)
	{
		$('#contentFP').html(group[$(this).attr('fieldtype')]);
	}
	
		$('.selectpicker').selectpicker({
		  style: 'btn-info',
		});
		$('.selectpicker2').selectpicker({
		  style: 'btn-info',
			width:100
		});
		$("input[id='maxcharacters']").TouchSpin({
				initval: 255,
                min: 0,
                max: 255,
                stepinterval: 1,
                maxboostedstep: 1,
                prefix: 'Character',
                verticalbuttons: true
        });
        $("input[id='heightupdown']").TouchSpin({
				initval: 100,
                min: 30,
                max: 500,
                stepinterval: 1,
                maxboostedstep: 1,
                postfix: 'px',
                verticalbuttons: true
        });
        $("input[id='maxsize']").TouchSpin({
        	 	initval: 200,
                min: 30,
                max: 9999,
                stepinterval: 1,
                maxboostedstep: 1,
                postfix: 'px',
                verticalbuttons: true
        });
        $('.date-picker').datepicker({
        	autoclose:true,
        	todayHighlight:true,
        	weekStart:0,
        	format:'dd-M-yyyy'
        });
        $('.allowedday').selectpicker({
		  style: 'btn-info'
		});
		$('.allowedhoursstart').selectpicker({
		  style: 'btn-info',
		  size:10
		});
		$('.allowedhoursend').selectpicker({
		  style: 'btn-info',
		  size:10
		});
       $(".form_datetime").datetimepicker({
        format: "dd-MM-yyyy - hh:ii",
        autoclose: true,
        todayBtn: true,
        startDate: "2013-02-14 10:00",
        minuteStep: 1
    	});
        $('.dropdown-sortable').sortable({
        	
        	helper: "clone"
        });
       	if(single===true)
		{
			return true;
		}
		console.log(single);
        $("#contentFP").mCustomScrollbar({
                theme:"3d-dark",
                axis:"y",
                autoHideScrollbar: false,
                documentTouchScroll: true,
                mouseWheel:{ axis: "y"},
                mouseWheel:{ preventDefault: true },
                contentTouchScroll: 10,
                documentTouchScroll: true
        });
        single=true;

});
$(document).on('click', '.allowedhoursstart',function()
{
	var s = $('.allowedhoursstart').selectpicker('val');
	var e = $('.allowedhoursend').selectpicker('val');
	if(s>e)
	{
		$('.allowedhoursend').selectpicker('val',s);
	}
});
$(document).on('click', '.allowedhoursend',function()
{
	var s = $('.allowedhoursstart').selectpicker('val');
	var e = $('.allowedhoursend').selectpicker('val');
	if(s>e)
	{
		$('.allowedhoursstart').selectpicker('val',e);
	}
});
$(document).on('click', '.tempActions', function () {
	single=true;
	swal({
		title: "Ajax request example",
		text: "Submit to run ajax request",
		type: "info",
		showCancelButton: true,
		closeOnConfirm: false,
		showLoaderOnConfirm: true
	},
		function () {
			setTimeout(function () {
				swal("Ajax request finished!");
				$(".selectedType").slideUp("normal", function () { 
					$('.scroll').html('');
					$('.scroll').html('<div id="contentFP" style="max-height: 600px;"></div>');
					$(this).remove(); });
					single=false;
			}, 0);
		});
});

$(document).on('click','.addmoreChoise',function(e)
{
	$(this).closest("li").after(getFieldChoise(1,'New item'));
});
$(document).on('click','.removeChoise',function(e)
{
	if($('.removeChoise').length >1)
	{
		var me = $(this).closest("li");
		$(me).remove();
	}
});
