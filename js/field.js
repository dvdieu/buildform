var popup =
  '<div class="zcOverlay" style="display: block;">' +
  '<div class="leftLay"></div>' +
  '<div class="topLay"></div>' +
  '<div class="centerLay"></div>' +
  '<div></div>' +
  '<div class="popupMainContainer ui-draggable" style="top: 21.5px; left: 606.5px; display: block;">' +
  '<div class="popup_header">' +
  '<h3 class="flLeft">Create Lookup Relation</h3>' +
  '<span class="popupClose"></span>' +
  '</div>' +
  '<div class="popupConentContainer magrinBottom" id="1234">' +
  '<div class="popupMainCont">' +
  '<input style="display:none;" type="text" name="referenceApplication">' +
  '<input style="display:none;" type="text" name="referenceForm">' +
  '<input style="display:none;" type="text" name="optionId">' +
  '<div class="choiceCont" style="position:relative;">' +
  '<div elname="chooseRelation" class="popupMenu">' +
  '<a relation="new" href="javascript:;" class="selected">New Relation</a>' +
  '<a relation="exist" href="javascript:;">Existing Relation</a>' +
  '</div>' +
  '<div elname="mainContent" style="display: block; min-height: 248px; border: 1px solid #ddd; padding:40px 30px 20px 30px; border-radius:2px; min-width:590px;">' +
  '<div class="nolookup">' +
  '<div class="nodatabg"></div>' +
  '<h3>You have no Application and Form to lookup</h3>' +
  '</div>' +
  '</div>' +
  '<div class="displayoptions" style="display:none">' +
  '<h4>Display Options</h4>' +
  '</div>' +
  '<div elname="displayTypeError" style="display:none; top:-70px; left:40%;" class="looupErro"></div>' +
  '<div align="center" class="displayAS">' +
  '<label>Display Type</label>' +
  '<div class="custormselectbox" style="width:120px;">' +
  '<select name="displayType" class="invisibledropdown" style="width: 195px;">' +
  '<option value="100">Dropdown</option>' +
  '<option value="101">Radio Button</option>' +
  '<option value="102">Multi Select</option>' +
  '<option value="103">Checkbox</option>' +
  '</select>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</div>' +
  '</div>' +
  '</div>' +
  '</div>' +
  '<div class="popupFooter popupFooterBroder">' +
  '<input type="button" class="PopButton1" value="Done">' +
  '<input type="button" class="PopButton2" value="Cancel">' +
  '</div>' +
  '</div>' +
  '</div>';


var data = new Array();
data.push({
  key: "singleline",
  value: '<li elname="fieldLi" fieldtype="singleline" labelname="Single Line" class="tempFrmWrapper nMedium">' +
  '<div class="ipadFieldMove"><small></small> </div>' +
  '<label class="labelName" elname="field-label"> Single Line </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" value="" style="width:200px;">' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div class="tempActions"> <span class="delIcon"></span> </div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "multiline",
  value: '<li elname="fieldLi" fieldtype="multiline" labelname="Multi Line" class="tempFrmWrapper nMedium" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small> </div>' +
  '<label class="labelName" elname="field-label">Multi Line</label>' +
  '<div class="tempContDiv">' +
  '<textarea class="text flLeft" style="width:200px;height:100px;"></textarea>' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div class="tempActions"><span class="delIcon"></span> </div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "email",
  value: '<li elname="fieldLi" fieldtype="email" labelname="Email" class="tempFrmWrapper nMedium" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Email </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" value="" style="width:200px;">' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "richtext",
  value: '<li elname="fieldLi" fieldtype="richtext" labelname="Rich Text" class="richtext tempFrmWrapper nMedium" style="opacity: 1; position: relative; left: 0px; top: 0px;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Rich Text </label>' +
  '<div class="tempContDiv"> <img align="absmiddle" src="./images/rt_editor_field_icon.png" title=""> </div>' +
  '<div class="imglock" style="display:none;"></div>' +
  '<div elname="deleteFieldEl" onclick="$(this).deleteField(event);" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "date",
  value: '<li elname="fieldLi" fieldtype="date" labelname="Date" class="tempFrmWrapper nMedium date" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Date </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" value="" style="width:200px;"> <span class="calendarIcon"></span>' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "datetime",
  value: '<li elname="fieldLi" fieldtype="datetime" labelname="DateTime" class="tempFrmWrapper nMedium date" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Date Time </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" value="" style="width:200px;"> <span class="calTimeIcon"></span>' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "dropdown",
  value: '<li elname="fieldLi" fieldtype="dropdown" labelname="Dropdown" class="tempFrmWrapper nMedium" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Drop Down </label>' +
  '<div class="tempContDiv">' +
  '<div id="lookuptype" class="dropdown">' +
  '<select class="flLeft" style="width:200px;">' +
  '<option>Choice 1</option>' +
  '<option>Choice 2</option>' +
  '<option>Choice 3</option>' +
  '</select>' +
  '</div>' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "radio",
  value: '<li elname="fieldLi" fieldtype="radio" labelname="Radio" class="tempFrmWrapper nMedium oneColumns" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Radio </label>' +
  '<div class="tempContDiv">' +
  '<div id="lookuptype">' +
  '<span>' +
  '<input type="radio" name="radio1">' +
  '<label>Choice 1</label>' +
  '</span>' +
  '<span>' +
  '<input type="radio" name="radio1">' +
  '<label>Choice 2</label>' +
  '</span>' +
  '<span>' +
  '<input type="radio" name="radio1">' +
  '<label>Choice 3</label>' +
  '</span>' +
  '<div elname="otherOption" style="display:none;">' +
  '<span>' +
  '<input type="radio" name="radio1">' +
  '<label>Others</label>' +
  '</span> ' +
  '</div>' +
  '</div>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '<div class="imglock" style="display:none;"></div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "checkbox",
  value: '<li elname="fieldLi" fieldtype="checkbox" labelname="Checkbox" class="tempFrmWrapper nMedium oneColumns" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Checkbox </label>' +
  '<div class="tempContDiv">' +
  '<div id="lookuptype">' +
  '<span>' +
  '<input type="checkbox">' +
  '<label>Choice 1</label>' +
  '</span>' +
  '<span>' +
  '<input type="checkbox" ' +
  '<label>Choice 2</label>' +
  '</span>' +
  '<span>' +
  '<input type="checkbox">' +
  '<label>Choice 3</label>' +
  '</span>' +
  '</div>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '<div class="imglock" style="display:none;"></div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "multiselect",
  value:
  '<li elname="fieldLi" fieldtype="multiselect" labelname="Multi Select" class="tempFrmWrapper nMedium" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Multi Select </label>' +
  '<div class="tempContDiv">' +
  '<div id="lookuptype">' +
  '<select multiple="multiple" class="flLeft" style="width:200px;min-height:50px;height:60px;">' +
  '<option value="">Choice 1</option>' +
  '<option value="">Choice 2</option>' +
  '<option value="">Choice 3</option>' +
  '</select>' +
  '</div>' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});


data.push({
  key: "number",
  value: '<li elname="fieldLi" fieldtype="number" labelname="Number" class="tempFrmWrapper nMedium">' +
  '<div class="ipadFieldMove"><small></small> </div>' +
  '<label class="labelName" elname="field-label"> Number </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" value="" style="width:200px;">' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div class="tempActions"> <span class="delIcon"></span> </div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});


data.push({
  key: "decimal",
  value: '<li elname="fieldLi" fieldtype="decimal" labelname="Decimal" class="tempFrmWrapper nMedium">' +
  '<div class="ipadFieldMove"><small></small> </div>' +
  '<label class="labelName" elname="field-label"> Decimal </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" value="" style="width:200px;">' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div class="tempActions"> <span class="delIcon"></span> </div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "percent",
  value: '<li elname="fieldLi" fieldtype="percent" labelname="Percent" class="tempFrmWrapper nMedium" style="opacity: 1; position: relative; left: 0px; top: 0px;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Percent </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" value="" style="width:200px;"> <span elname="currSymbol" style="margin-left:-25px;padding:7px 0 0 7px; float:left;">%</span>' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "currency",
  value: '<li elname="fieldLi" fieldtype="currency" labelname="Currency" class="tempFrmWrapper nMedium" style="opacity: 1; position: relative; left: 0px; top: 0px;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Currency </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" value="" style="width:200px;"> <span elname="currSymbol" style="margin-left:-40px;padding:7px 0 0 7px; float:left;">VND</span>' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "url",
  value: '<li elname="fieldLi" fieldtype="url" labelname="Url" class="tempFrmWrapper nMedium" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Url </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" value="" style="display:block;width:140px;">' +
  '<div class="imglock" style="display:none;"></div>' +
  '<div class="clearBoth"></div>' +
  '<input type="text" class="flLeft" name="linkNameRequired" placeholder="Link Name" style="width:140px;margin-top:10px;display:block;">' +
  '<div class="clearBoth"></div>' +
  '<input type="text" class="flLeft" name="titleRequired" placeholder="Title" style="width: 140px; margin-top: 10px; display: none;"> </div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "image",
  value: '<li elname="fieldLi" fieldtype="image" labelname="Image" class="tempFrmWrapper nMedium" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> Image </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" value="" style="width:200px;">' +
  '<div class="clearBoth"></div>' +
  '<input type="text" class="flLeft" name="titleRequired" placeholder="Title" style="width: 200px; margin-top: 10px; display: none;">' +
  '<div class="clearBoth"></div>' +
  '<input type="text" class="flLeft" name="altRequired" placeholder="Alt Text" style="width: 200px; margin-top: 10px; display: none;">' +
  '<div class="clearBoth"></div>' +
  '<input type="text" class="flLeft" name="linkRequired" placeholder="Link" style="width: 200px; margin-top: 10px; display: none;">' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "decisionbox",
  value: '<li elname="fieldLi" fieldtype="decisionbox" labelname="Decisionbox" class="tempFrmWrapper nMedium decesion" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<div class="tempContDiv_chk">' +
  '<input type="checkbox"> </div>' +
  '<label class="labelName flLeft desCheck" elname="field-label" style="width:auto;"> <span class="flLeft">Decision box</span> </label>' +
  '<div class="imglock" style="display:none;"></div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "fileupload",
  value: '<li elname="fieldLi" fieldtype="fileupload" labelname="Fileupload" class="tempFrmWrapper nMedium zc-fb-fileupload" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label"> File upload </label>' +
  '<div class="tempContDiv">' +
  '<input class="flLeft" type="file" style="width:200px;">' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div elname="deleteFieldEl" onclick="$(this).deleteField(event);" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "addnotes",
  value: '<li elname="fieldLi" fieldtype="addnotes" labelname="addnotes" class="tempFrmWrapper nMedium addNotestype" style="opacity: 1;">' +
  '<div class="ipadFieldMove"><small></small>' +
  '</div>' +
  '<label class="labelName" elname="field-label" style="width:auto;font-weight:normal"> Add your Note here ... </label>' +
  '<div class="tempContDiv"></div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "signature",
  value:
  '<li elname="fieldLi" fieldtype="signature" labelname="Signature" seq_no="6" class="signature tempFrmWrapper nMedium" style="opacity: 1;">' +
  '<div class="ipadFieldMove"></div>' +
  '<label class="labelName" elname="field-label"> Signature </label>' +
  '<div class="tempContDiv">' +
  '<div style="float:left;"><img src="./images/signatures.png">' +
  '</div>' +
  '<div class="imglock" style="display:none;"></div>' +
  '</div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});

data.push({
  key: "users",
  value: '<li elname="fieldLi" fieldtype="users" labelname="users" class="tempFrmWrapper nMedium" style="opacity: 1;">' +
  '<div class="ipadFieldMove"></div>' +
  '<label class="labelName" elname="field-label"> Users </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" style="width:200px;" value=""><b class="crmLeadIcon flLeft"></b>' +
  '<p class="instruction"> </p>' +
  '</div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});


data.push({
  key: "lookup",
  value: '<li elname="fieldLi" fieldtype="users" labelname="users" class="tempFrmWrapper nMedium" style="opacity: 1;">' +
  '<div class="ipadFieldMove"></div>' +
  '<label class="labelName" elname="field-label"> Users </label>' +
  '<div class="tempContDiv">' +
  '<input type="text" class="flLeft" style="width:200px;" value=""><b class="crmLeadIcon flLeft"></b>' +
  '<p class="instruction"> </p>' +
  '</div>' +
  '<div elname="deleteFieldEl" class="tempActions"><span class="delIcon"></span>' +
  '</div>' +
  '<div class="clearBoth"></div>' +
  '</li>'
});